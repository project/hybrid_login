# Hybrid Login module

## Overview

This module is designed to generate a custom login block for use on the Drupal user login page. Modifying the settings in this module can alter the following:

* Show the Drupal default login, your external login service of choice, or both.
* Add a title for the Hybrid Login block
* Add a description to the Hybrid Login block, informing users how they can access their account using an external login service.
* Deny/allow access to the password reset page
* Add a description to the password reset page, informing users how they can reset their password on Drupal or selected login service

## Installation

There are a couple steps required for enabling features of the Hybrid Login module.

1. Install the module into your project

    `composer require drupal/hybrid_login`

1. Enable the module via the Drupal extension page or drush command

    `drush en hybrid_login`

1. Place the "Hybrid Login" block on the Block Layout page, located at Structure > Block layout
    
    a. It is recommended to place the block in the "Content" region, above the "Main page content" and below the "Admin tabs", if applicable.

1. Configure the block with the recommended settings

    a. **Display title:** unchecked

    b. **Show for the listed pages:** `/user/login`

1. Visit the admin settings page for Hybrid Login, located at Configuration > People > Login settings

    `/admin/config/people/hybrid_login`

1. It is recommended to clear cache after changing any Hybrid Login settings. See [Usage](#usage) for explanation of different settings

## Usage

This section gives a breif overview of different settings available in this module, as well as best practices for using this module on your site.

### Hybrid Login Settings
These settings can be located at Configuration > People >Login settings

`/admin/config/people/hybrid_login`

* **Hide default Drupal login block**
    * Type: Checkbox
    * This setting can hide the default Drupal login block that is displayed on `/user/login`. This setting does not modify the Hybrid Login block. To hide the Hybrid Login block, please reference [Show or hide the Hybrid Login block](#show-or-hide-the-hybrid_login-block) below.

* **Login title**
    * Type: Textbox
    * Customize the title that is displayed on the Hybrid Login block. Defaults to "Login".

* **Description on user login page** 
    * Type: Textarea
    * Add a description to the Hybrid Login block. This can be used for login instructions, site announcements, or informational materials. This is a basic text area so no text formatting is available.

* **Login Service Logo** 
    * Type: Image upload
    * Provide an optional image of the external login service you are using. Accepted file types: jpg, jpeg, png. The maximum file size is specific to your site settings.

* **Login button text**
    * Type: Textbox
    * Customize the text that is displayed on the login button. Defaults to "Login".

* **Login button path**
    * Type: Textbox
    * Customize the URL path that will direct the user to the external login service. Use a relative path only. This URL is dependent on the SAML Auth module and is typically set to `/saml/login`. Depending on your site settings, this URL may be different.

* **Show password reset link**
    * Type: Checkbox
    * This setting will disable the default password reset page on your site ( `/user/password` ). In doing so, password reset links referenced on your site should also be hidden.

* **Description on password reset page**
    * Type: Textarea
    * Only available if *Show password reset link* is enabled. Add a description to the password reset form. This can be used for password reset instructions for Drupal or external login services. It can also provide site announcements or informational materials. This is a basic text area so no text formatting is available.

### Show or hide the Hybrid Login block

This is not a setting that can be changed within the Hybrid Login settings. Since this is a block that was placed during the module [Installation](#installation) steps, you will need to configure the block on the block layout page.

1. Navigate to the Block layout page, located at Structure > Block layout

1. Locate the "Hybrid Login" block under the Content region. Click the dropdown next to "Configure" and select "Disable" or "Enable"

    a. You may need to re-order the block if you enable the block again.

## Uninstalling this module

You can uninstall the module using the following methods

* Using Drupal's "Uninstall Module" page, located at `/admin/modules/uninstall`
* Using Drush `drush pmu hybrid_login`

Don't forget to remove the module from the *composer.json* file after uninstalling

* `composer remove civicactions/hybrid_login`
