<?php

namespace Drupal\hybrid_login\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Get saved configuration
    $hybrid_login_settings =  \Drupal::config('hybrid_login.settings');

    // Deny access to '/user/password' if "Show password reset" option is not selected or Drupal login form is hidden
    if ($hybrid_login_settings->get('show_password_reset') === 0 || $hybrid_login_settings->get('hide_drupal_login') === 1) {
      if ($route = $collection->get('user.pass')) {
        $route->setRequirement('_access', 'FALSE');
      }
    }

    // Deny access to '/user/register' if "Show create account link" option is not selected or Drupal login form is hidden
    if ($hybrid_login_settings->get('show_create_account') === 0 || $hybrid_login_settings->get('hide_drupal_login') === 1) {
      if ($route = $collection->get('user.register')) {
        $route->setRequirement('_access', 'FALSE');
      }
    }
  }

}