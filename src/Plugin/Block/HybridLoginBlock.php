<?php

namespace Drupal\hybrid_login\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\file\Entity\File;

/**
 * Provides a customizable login block to place on the login page.
 *
 * @Block(
 *   id = "hybrid_login_block",
 *   admin_label = @Translation("Hybrid Login"),
 *   category = @Translation("Forms"),
 * )
 */
class HybridLoginBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $hybrid_login_settings =  \Drupal::config('hybrid_login.settings');
    $hide_drupal_login = $hybrid_login_settings->get('hide_drupal_login');
    $login_title = $hybrid_login_settings->get('login_title');
    $login_description = $hybrid_login_settings->get('login_description');
    $login_button_text = $hybrid_login_settings->get('login_button_text') ?: 'Login';
    $login_url_path = $hybrid_login_settings->get('login_url_path');
    $login_logo = '';
    
    // Get login service logo.
    if (!empty($hybrid_login_settings->get('login_logo')[0])) {
      if ($file = File::load($hybrid_login_settings->get('login_logo')[0])) {
        $login_logo = [
          '#theme' => 'image_style',
          '#style_name' => 'medium',
          '#uri' => $file->getFileUri(),
        ];
      }
    }

    return [
      '#theme' => 'hybrid_login_block',
      '#hide_drupal_login' => $hide_drupal_login,
      '#login_title' => $login_title,
      '#login_description' => [
        '#markup' => $login_description,
      ],
      '#login_logo' => $login_logo,
      '#login_button_text' => $login_button_text,
      '#login_url_path' => $login_url_path,
    ];
  }

}