<?php

namespace Drupal\hybrid_login\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

class HybridLoginConfigurationForm extends ConfigFormBase {

  const SETTINGS = 'hybrid_login.settings';

  public function getFormId() {
    return 'hybrid_login_configuration_form';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  // Generate form with fields
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get initial form values, if any
    $config = $this->config(static::SETTINGS);

    // Instructions for disabling the Hybrid Login block on the block layout page
    $form['hybrid_login_block_details'] = [
      '#type' => 'item',
      '#title' => $this->t('To hide the Hybrid Login block, disable it on the <a href="/admin/structure/block">block layout page</a>.'),
    ]; 
    // Login form to display on login page
    $form['hide_drupal_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Drupal login block'),
      '#description' => t("If you have other validation services enabled, like CAPTCHA, you will need to disable those separately."),
      '#default_value' => $config->get('hide_drupal_login'),
      '#tree' => FALSE,
    ]; 
    // Show create account link on user login page 
    $form['show_create_account'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show create account link'),
      '#description' => t("Leave this unchecked if you are only allowing new accounts through an external service."),
      '#default_value' => $config->get('show_create_account'),
      '#tree' => FALSE,
    ]; 
    // Login page title
    $form['login_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login page title'),
      '#default_value' => $config->get('login_title'),
    ];
    // Login instructions
    $form['login_description'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Description on user login page'),
        '#description' => t("Show a message explaining rules for login."),
        '#default_value' => $config->get('login_description'),
    ];
    // Login service logo image
    $form['login_logo'] = [
      '#type' => 'managed_file',
      '#title' => t('Login service logo'),
      '#upload_validators' => array(
          'file_validate_extensions' => array('png jpg jpeg'),
      ),
      '#upload_location' => 'public://',
      '#progress_message' => 'Uploading file...',
      '#description' => t("Accepted file types: png, jpg, jpeg"),
      '#default_value' => $config->get('login_logo') ?: '',
    ];
    // Text for login button
    $form['login_button_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Login button text'),
        '#default_value' => $config->get('login_button_text'),
    ];
    // Path for SAML Login
    $form['login_url_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login button path'),
      '#description' => t("Relative path for the SAML Auth login link."),
      '#default_value' => $config->get('login_url_path'),
    ];
    // Show password reset link
    $form['show_password_reset'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show password reset link'),
      '#default_value' => $config->get('show_password_reset'),
      '#tree' => FALSE,
      '#states' => array(
        // Hide when Drupal login fields are hidden
        'visible' => array(
          ':input[name="hide_drupal_login"]' => array(
            'checked' => FALSE,
          ),
        ),
      ),
    ];
    // Password reset instructions
    $form['password_reset_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description on password reset page'),
      '#description' => t("Show a message explaining rules for password reset."),
      '#default_value' => $config->get('password_reset_description'),
      '#states' => array(
        'visible' => array(
          // Hide when Drupal login fields are hidden
          ':input[name="hide_drupal_login"]' => array(
            'checked' => FALSE,
          ),
          // Show when "Show Password Reset" field is selected
          ':input[name="show_password_reset"]' => array(
            'checked' => TRUE,
          ),
        ),
      ),
    ];

    return parent::buildForm($form, $form_state);
  }

  // Submit form
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $submitted_values = $form_state->cleanValues()->getValues();
    $config = $this->config(static::SETTINGS);

    // Save the image permanantly
    $login_logo = $form_state->getValue('login_logo');
    if ($login_logo != $config->get('login_logo')) {
        if (!empty($login_logo[0])) {
            $file = File::load($login_logo[0]);
            $file->setPermanent();
            $file->save();
        }
    }

    $this->config(static::SETTINGS)
    // Set the submitted configuration setting.
      ->set('hide_drupal_login', $form_state->getValue('hide_drupal_login'))
      ->set('show_create_account', $form_state->getValue('show_create_account'))
      ->set('login_title', $form_state->getValue('login_title'))
      ->set('login_description', $form_state->getValue('login_description'))
      ->set('login_logo', $form_state->getValue('login_logo'))
      ->set('login_button_text', $form_state->getValue('login_button_text'))
      ->set('login_url_path', $form_state->getValue('login_url_path'))
      ->set('show_password_reset', $form_state->getValue('show_password_reset'))
      ->set('password_reset_description', $form_state->getValue('password_reset_description'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}